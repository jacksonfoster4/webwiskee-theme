<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header(); ?>

	<main id="primary" class="site-main">
    <?php
	if ( have_posts() ) :

		if ( is_home() && ! is_front_page() ) : ?>
			<header>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			</header>

		<?php
		endif;

		/* Start the Loop */
        ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12" style="min-height: 60vh; background-image: url('https://webwiskee.com/wp-content/uploads/2021/04/Simpson_Portraits_WEB_SFS_3469.jpg'); background-size: cover; background-repeat: no-repeat">
                </div>
            </div>
        </div>
        <div class="container" id="ww-blog-container">
            <div class="row justify-content-center">
                <div class="col-md-8 p-3 pt-0"><?php
                    while ( have_posts() ) : the_post();

                        /*
                            * Include the Post-Format-specific template for the content.
                            * If you want to override this in a child theme, then include a file
                            * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                            */
                        get_template_part( 'template-parts/content', get_post_format() );
                    
                    endwhile;
                    the_posts_pagination(); ?>
                </div>
                <div style="margin-top: 60px" class="col-md-2 ms-l-5 ms-xl-5 text-center">
                    <div class="blog-social-icons mb-4">
                        <div class="dashicons-wrapper-dark">
                            <a href="<?php echo get_option('ww_facebook_link'); ?>" target="_blank" class="dashicons dashicons-facebook-alt"></a>
                        </div>
                        <div class="dashicons-wrapper-dark">
                            <a href="<?php echo get_option('ww_instagram_link'); ?>" target="_blank" class="dashicons dashicons-instagram"></a>
                        </div>
                        <div class="dashicons-wrapper-dark">
                            <a href="<?php echo get_option('ww_twitter_link'); ?>" target="_blank" class="dashicons dashicons-twitter"></a>
                        </div>
                    </div>

                    <?php 
                    get_search_form();
                    get_template_part( 'template-parts/recent-posts'); ?>

                </div>
            </div>
        </div>
		
    <?php
	else :

		get_template_part( 'template-parts/content', 'none' );

	endif; ?>

	</main><!-- #primary -->

<?php
get_footer();
