<?php
/**
 * Displays the frequency selector for the pricing tables
 *
 * This template can be overridden by copying it to yourtheme/wp-ultimo/signup/pricing-table/frequency-selector.php.
 *
 * HOWEVER, on occasion WP Ultimo will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @author      NextPress
 * @package     WP_Ultimo/Views
 * @version     1.0.0
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly
}

?>

<?php if (WU_Settings::get_setting('enable_price_3') || WU_Settings::get_setting('enable_price_12')) : ?>

<ul class="wu-plans-frequency-selector">

  <?php

  $prices = array(
    1  => __('Monthly', 'wp-ultimo'), 
    12 => __('Annually', 'wp-ultimo'), 
  );

  $first = true;

  foreach($prices as $type => $name) : 

    if (!WU_Settings::get_setting('enable_price_' . $type)) continue; 

  ?>
  <li>
    <a style="padding: 1rem 2rem !important;"class="<?php echo $first ? 'active first' : ''; ?> btn d-inline" data-frequency-selector="<?php echo $type; ?>" href="#">
      <?php echo $name; ?>
    </a>
    <?php if ($type == 12): ?>
      <span class="annual-billing-cta">Get 2 months free<br>with annual billing</span>
    <?php endif; ?>
  </li>
  <?php $first = false; endforeach; ?>
</ul>
<span class="annual-billing-cta mobile mt-2">Get 2 months free<br>with annual billing</span>
<?php endif; ?>
