<?php
/**
 * Displays each individual plan on the pricing table loop
 *
 * This template can be overridden by copying it to yourtheme/wp-ultimo/signup/plan.php.
 *
 * HOWEVER, on occasion WP Ultimo will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @author      NextPress
 * @package     WP_Ultimo/Views
 * @version     1.0.0
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly
}

?>

<?php

/**
 * Set plan attributes
 * @var string
 */
$plan_attrs = '';

foreach(array(1, 12) as $type) {
  $price = ( ( (float) $plan->{"price_".$type}) / $type );
  if(substr(sprintf("%.2f", $price), -2) == '00'){
    $price = floatval($price); //remove trailing '00'
  }
  else {
      $price = number_format(round($price, 2), 2);
  }
  $formatted_price = $plan->free ? __('Free!', 'wp-ultimo') : str_replace(get_wu_currency_symbol(), '', $price);
  $plan_attrs .= " data-price-$type='$formatted_price'";

} // end foreach;

$plan_attrs = apply_filters("wu_pricing_table_plan", $plan_attrs, $plan);
$button_attrubutes = apply_filters('wu_plan_select_button_attributes', "", $plan, $current_plan);
$button_label = $current_plan != null && $current_plan->id == $plan->id ? __('This is your current plan', 'wp-ultimo') : __('Select Plan', 'wp-ultimo');
$button_label = apply_filters('wu_plan_select_button_label', $button_label, $plan, $current_plan);
?>

<div id="plan-<?php echo $plan->id; ?>" data-plan="<?php echo $plan->id; ?>" <?php echo $plan_attrs; ?>class="plan wu-plan plan-tier col-sm-<?php echo $columns;  ?> <?php echo $plan->top_deal ? 'orange': null; ?> col-xs-12 px-0">
    <div class="plan-container">
        <div class="spacer"></div>
        <div class="plan-content">
            <h2><?php echo $plan->title; ?></h2>
            <p><?php echo $plan->description; ?></p>
            <div class="price">
                <div class="d-inline"><?php echo get_wu_currency_symbol(); ?></div>
                <div class="plan-price d-inline">
                    <?php echo $plan->price_1; ?>
                </div>
            </div>
            <button type="submit" name="plan_id" class="button button-primary button-next" value="<?php echo $plan->id; ?>" <?php echo $button_attrubutes; ?>>
                <?php echo $button_label; ?>
            </button>
            <ul class="specs">
                
                <?php 
                /**
                 * 
                 * Display quarterly and Annually plans, to be hidden
                 * 
                 */
                $prices_total = array(
                    12 => __('annually', 'wp-ultimo'), 
                );

                foreach ($prices_total as $freq => $string) {
                
                    $text = sprintf(__('%s, billed %s', 'wp-ultimo'), get_wu_currency_symbol().''.$plan->{"price_$freq"}, $string );
                    
                    if ($plan->free || $plan->is_contact_us()) echo "<li class='total-price total-price-$freq'>-</li>";
                    
                    else echo "<li class='total-price total-price-$freq'>$text</li>";
                
                }

                foreach ($plan->get_pricing_table_lines() as $line) : ?>

                    <li class="spec"><?php echo $line; ?></li>

                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>


