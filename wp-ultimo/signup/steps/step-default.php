<?php
/**
 * This is the default template used for steps defined ont he steps array 
 *
 * This template can be overridden by copying it to yourtheme/wp-ultimo/signup/steps/step-default.php.
 *
 * HOWEVER, on occasion WP Ultimo will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @author      NextPress
 * @package     WP_Ultimo/Views
 * @version     1.0.0
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly
}

?>

<div class="wu-setup-content wu-content-<?php echo $signup->step; ?>">

  <!-- <p class="message" style="width: 320px; margin-left: auto; margin-right: auto; box-sizing: border-box;">
    Please enter your username or email address. You will receive a link to create a new password via email.
  </p> -->
  
  <form name="loginform" id="loginform" method="post">

    <?php 

    foreach ($fields as $field_slug => $field) {
      /**
       * Prints each of our fields using a helper function
       */
        if($field_slug == "url_preview"){
           $field['content'] = "<p id='wu-your-site-block'><small>The working title of your site will be</small><br><strong id='wu-your-site'>yoursite</strong>.<span id='wu-site-domain'>webwiskee.com</span></p>";
;            
        }
      if($field_slug == "custom_domain_notice"){
          $field['content'] = "<p><small>Choose something simple. You're the only one who is going to see it!<br><br></small></p>";
      }

      wu_print_signup_field($field_slug, $field, $results);

    } // end foreach;
    //echo var_dump(get_user_meta(4));

    ?>

    <?php do_action("wp_ultimo_registration_step_$signup->step"); ?>

  </form>
  <script>
      document.addEventListener("DOMContentLoaded", (e) => {
          let modal = document.getElementById("loading-modal")
          document.body.appendChild(modal)

          let accountLoginForm = document.querySelector(".wu-content-account #loginform")
          if(!accountLoginForm){ return }

          accountLoginForm.addEventListener("submit", (e) => {
              document.querySelector('#loading-modal').classList += " visible";
          })


      })
      // domain validation
      window.addEventListener("load", (e) => {
          let blogTitle = document.querySelector("#blog_title") 
          if(!blogTitle) { return }

          blogTitle.addEventListener("input", (e) => {
              let blogName = document.querySelector("#blogname") 

              let allowedCharacters = 'abcdefghijklmnopqrstuvwxyz'.split("")
              let allowedNumbers = '1234567890'.split("")
              let allowedSpecial = '-'.split("")

              // check if all characters are valid
              let value = blogTitle.value.split(" ").join('').toLowerCase()
              value = value.split("").filter((char) => {
                  if(allowedCharacters.includes(char) || allowedNumbers.includes(char) || allowedSpecial.includes(char)){
                      return true
                  }
                  return false
              })

              // domain can't begin or end with special character
              let invalid = allowedSpecial.includes(value[0]) || allowedSpecial.includes(value[value.length-1])
              while(invalid){
                  if(allowedSpecial.includes(value[0])){
                     value.shift()
                  }
                  else if(allowedSpecial.includes(value[value.length-1])){
                     value.pop()
                  }
                  else {
                     invalid = false
                  }
              }

              blogName.value = value.join('') 
              if(blogName.value.length == 0){ 
                  document.querySelector("#wu-your-site").innerHTML = 'yoursite'
              }
              else {
                  document.querySelector("#wu-your-site").innerHTML = blogName.value
              }

          })
      })
  </script>


</div>
<style>
    #loading-modal {
        display: none;
        background-color: rgba(0,0,0,0.3);
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
        position: absolute;
    }
    #loading-modal.visible {
        display: flex;
        flex-direction: column;
        justify-content:center;
        align-items: center;
    }
    #loading-modal .modal-container {
        background-color: white;
        border-radius: 10px;
        padding: 2rem;
        max-width: 45%;
        z-index: 1001;
    }
    #blogname-field, #domain_option-field {
        display: none
    }
</style>
<div id="loading-modal">
    <div class="modal-container">
        <h1>Your website is being created! This may take a minute.</h1>
    </div>
</div>
