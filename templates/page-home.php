<?php
/*
 * Template Name: Home Page
 */
get_header();
?>	

<link href="<?php echo get_theme_stylesheet('home'); ?>" rel="stylesheet"/> 
<!-- hero with watermark -->
<div class="jumbotron-fluid home-hero" style="background-image: url('/wp-content/uploads/2021/04/whiskey-cover.jpg')">
    <div class="container">
        <div class="row justify-content-md-end">
            <div class="jumbotron-col col-lg-7 col-md-12 col-sm-12">
                <div class="hero-content float-end">
                    <h2 class="mb-5">Finally.</h2>
                    <h2 class="mb-3">A DIY WEBSITE
                        BUILDER<br>POWERED BY WORDPRESS</h2>
                    <p>
                        WebWiskee is an affordable, DIY website-builder that makes it easier than ever to take your shot and build the business you’ve always wanted.
                    </p>
<div class="wp-block-button dark mb-4 hustle-cta-button" form-id="1"><a form-id="1"class="wp-block-button__link no-border-radius">Join the Waitlist</a></div>
                    <div class="form-1 hustle-cta hustle-ui hustle-inline hustle-palette--gray_slate hustle_module_id_1 module_id_1 hustle-show" style="display: none" data-id="1" data-render-id="0" data-tracking="enabled" data-intro="no_animation" data-sub-type="shortcode" style="opacity: 1;">
                        <div class="hustle-inline-content">
                        <div class="hustle-optin hustle-optin--default">
                            <div class="hustle-success" data-close-delay="false" style="display: none;">
                                <span class="hustle-icon-check" aria-hidden="true"></span>
                                <div class="hustle-success-content"></div>
                            </div>
                            <div class="hustle-layout">
                                <div class="hustle-layout-body">
                                    <form class="hustle-layout-form" novalidate="novalidate">
                                    <div class="hustle-form hustle-form-inline">
                                        <div class="hustle-form-fields hustle-proximity-joined">
                                            <div class="hustle-field hustle-field-icon--static hustle-field-required opt-in-label"><label for="hustle-field-email-module-1" id="hustle-field-email-module-1-label" class="hustle-screen-reader">Enter your email...</label><input id="hustle-field-email-module-1" onfocus="hidePlaceholder(this)" type="email" class="hustle-input " name="email" value="" aria-labelledby="hustle-field-email-module-1-label" data-validate="1" data-required-error="Your email is required." data-validation-error="Please enter a valid email."><span class="hustle-input-label" aria-hidden="true"><span class="hustle-icon-email"></span><span>Enter your email...</span></span></div>
                                            <button class="hustle-button hustle-button-submit " type="submit" aria-live="polite" data-loading-text="Form is being submitted, please wait a bit."><span class="hustle-button-text">Keep Me Posted</span><span class="hustle-icon-loader hustle-loading-icon" aria-hidden="true"></span></button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="hustle_module_id" value="1"><input type="hidden" name="post_id" value="174"><input type="hidden" name="hustle_sub_type" value="shortcode">
                                    <div class="hustle-error-message" style="display: none;" data-default-error="Something went wrong, please try again."></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="watermark" src="/wp-content/uploads/2021/04/Rings-Watermark-White-20.png">
    <div class="overlay"></div>
</div>

<h3 class="has-text-align-center has-custom-size" style="font-size:48px">Powered by WordPress, the most trusted platform in<br>the world, WebWiskee won't just help you build your<br>website, we'll help you build your business.</h3>

<!-- image links -->
<div class="container py-5 image-links">
    <div class="row justify-content-md-center">
        <a href="/about/" class="col-lg-3 col-md-4 my-2 p-3">
            <div class="image-link-col" style="background-image: url('/wp-content/uploads/2021/04/Simpson_Portraits_WEB_SFS_3430.jpg')">
                <div class="image-link-col-text py-3">
                    <h2 class="m-0">About</h2>
                </div>
            </div>
        </a>
        <a href="/how-it-works/" class="col-lg-3 col-md-4 my-2 p-3">
            <div class="image-link-col" style="background-image: url('/wp-content/uploads/2020/11/guide-to-adding-keywords-to-content-webwiskee.png')">
                <div class="image-link-col-text py-3">
                    <h2 class="m-0">How It Works</h2>
                </div>
            </div>
        </a>
        <a href="/blog" class="col-lg-3 col-md-4 my-2 p-3">
            <div class="image-link-col" style="background-image: url('/wp-content/uploads/2021/04/Simpson_Portraits_WEB_SFS_3467-2.jpg')">
                <div class="image-link-col-text py-3">
                    <h2 class="m-0">Blog</h2>
                </div>
            </div>
        </a>
    </div>
</div>

<!-- the best of both worlds -->
<div class="container-fluid p-0 two-column-cover-image best-of-both-worlds">
    <div class="row w-100 m-0">
        <div class="col-md-6 image-col" style="background-image: url('/wp-content/uploads/2021/04/AdobeStock_401660028.jpeg');"></div>
        <div class="col-md-6 content-col" style="flex-direction: column">
            <h2>The Best of Both<br>Worlds!</h2>
        </div>
    </div>
</div>

<!--- 2 choices, until now...-->
<div class="container-fluid px-5 text-center h-50 choices-until-now">
    <div class="pb-4 intro-heading">
        <h4>
            Until now, business owners had only 2 choices for building a website:
        </h4>
    </div>
    <div class="row w-100 m-0 justify-content-lg-center">
        <div class="col-lg-3" style="background-image: url('/wp-content/uploads/2021/04/WebWiskee-Logo-Final-12-15-Transparent-Background-Web-Rings.png');">
            <p>
                Spend thousands on<br>a custom website
            </p>
        </div>
        <div class="col-lg-2">
            <div class="or"><em>-- OR --</em></div>
        </div>
        <div class="col-lg-3" style="background-image: url('/wp-content/uploads/2021/04/WebWiskee-Logo-Final-12-15-Transparent-Background-Web-Rings.png');">
            <p>
                Stay trapped on an<br>inflexible DIY platform
            </p>
        </div>
    </div>
    <div class="container pt-5 mt-3">
        <div class="row justify-content-md-center">
            <div class="col-md-10">
                <h2>Cheers!<br>WebWiskee has solved that problem!</h2>
                <p>
                    WebWiskee combines the ease of use and beautiful designs of a Do-It-Yourself website builder with the power,
                    findability, and robust tools available through Wordpress, and blends it all into one affordable platform.
                </p>
            </div>
        </div>
    </div>
</div>

<!-- why wordpress -->
<div class="container px-3 py-5 why-wordpress" style="overflow: hidden">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="w-100">
                <h2 class="w-100 text-center">Why Wordpress?</h2>
                <p>
                    WordPress is the gold standard for website performance. Google loves it, loads of customizations are available, and WordPress websites can easily grow as your business grows.<br><br>
                    The trouble is, WordPress can be hard for non-programmers to use, but without it, websites can't effectively grow and scale as your business does. There were no options for
                    robust businesses other than hiring an expensive programmer or making do with a lesser website. Until now!
                </p>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-7">
                    <h4 class="mt-4">We know that a bad website can mean a rough start for your business.</h4><br>
                    <p class="mb-3 p-0">
                        Most of the time you get left with:
                        </p><ul class="m-0 p-0">
                            <li>Expensive packages that you don't need</li>
                            <li>A site that Google couldn't find with a flashlight and a map</li>
                            <li>Customer service that drives you to drink</li>
                            <li>An inability to scale or edit your website</li>
                            <li>No idea how to target your ideal customer</li>
                            <li>A total lack of strategy</li>
                            <li>Headaches rather than celebrations</li>
                        </ul>
                    <p></p>
                </div>
                <div class="col-md-5">
                    <img id="wp-logo" src="/wp-content/uploads/2021/04/Wordpress-WW-Brand-Blue.png">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- flip cards -->
  <div class="container flip-cards">
      <h2 class="intro-heading">Who is WebWiskee for?</h2>
      <div class="row mt-5 justify-content-center">
          <div class="flip-card-col col-sm-12 col-md-8 col-lg-3 p-3">
              <div class="flip-card">
                  <div class="flip-card-inner">
                    <div class="flip-card-front" style="background-image: url('/wp-content/uploads/2020/11/internal-linking-best-practices.png')">
                      <div class="flip-card-text-container py-3">
                          <h2 class="m-0">Startup Businesses</h2>
                      </div>
                      <span class="dashicons dashicons-image-rotate"></span>
                    </div>
                    <div class="flip-card-back p-5">
                      You're building your empire, and you only want to build your website once. Getting it right the first time means your website will grow as your business does.
                    </div>
                  </div>
                </div>
          </div>
          <div class="flip-card-col col-sm-12 col-md-8 col-lg-3 p-3">
              <div class="flip-card">
                  <div class="flip-card-inner">
                    <div class="flip-card-front" style="background-image: url('/wp-content/uploads/2020/11/short-tail-vs-long-tail-keywords-1.png')">
                      <div class="flip-card-text-container py-3">
                          <h2 class="m-0">Scaling Businesses</h2>
                      </div>
                      <span class="dashicons dashicons-image-rotate"></span>
                    </div>
                    <div class="flip-card-back p-4">
                        You’ve built a starter website and now it’s time for something more robust with tools to make your life easier that will help you turn visitors into customers.                    </div>
                  </div>
                </div>
          </div>
      </div>
      <div class="row justify-content-md-center">
          <div class="flip-card-col col-sm-12 col-md-8 col-lg-3 p-3">
              <div class="flip-card">
                  <div class="flip-card-inner">
                    <div class="flip-card-front" style="background-image: url('/wp-content/uploads/2021/04/Simpson_Portraits_WEB_SFS_3430.jpg')">
                      <div class="flip-card-text-container py-3">
                          <h2 class="m-0">Niche Industries</h2>
                      </div>
                      <span class="dashicons dashicons-image-rotate"></span>
                    </div>
                    <div class="flip-card-back p-4">
                        We are building turn-key solutions for different industries like podcasters, coaches, stylists, or vacation rental owners. Let us know about how we can help your industry.                    </div>
                  </div>
                </div>
          </div>
          <div class="flip-card-col col-sm-12 col-md-8 col-lg-3 p-3">
              <div class="flip-card">
                  <div class="flip-card-inner">
                    <div class="flip-card-front" style="background-image: url('/wp-content/uploads/2021/04/Simpson_Portraits_WEB_SFS_3448.jpg')">
                      <div class="flip-card-text-container py-3">
                          <h2 class="m-0">Agencies</h2>
                      </div>
                      <span class="dashicons dashicons-image-rotate"></span>
                    </div>
                    <div class="flip-card-back p-4">
                        Your digital marketing agency and marketing design company needs a solution that allows quick WordPress builds that your clients can manage on their own.                    </div>
                  </div>
                </div>
          </div>
      </div>
  </div>

<p class="has-text-color" style="color:#363d44"><p class="has-text-color has-custom-lineheight ext-max-width ext-maxw-align-center" style="color:#363d44;line-height:1.3;max-width:1416px">Being an entrepreneur takes guts, vision, and the drive to TAKE YOUR SHOT. Amazingly, there just aren't that many resources out there designed to help business owners with the nuts and bolts, the strategy, the marketing, and the big picture for how to grow your company. Sure, a magazine might have a billionaire on the cover, but are they actually helping the owner of the corner pub to boost sales?</p></p>

<p class="has-text-color" style="color:#363d44"><p class="has-text-color has-custom-lineheight ext-max-width ext-maxw-align-center" style="color:#363d44;line-height:1.3;max-width:1416px">We love business owners and want to help you to stand out from the competition, to attract your ideal customers, to build sales, to communicate, and to connect. We are building loads of resources to help entrepreneurs build their empires.</p></p>

<p class="has-text-color" style="color:#363d44"><p class="has-text-color has-custom-lineheight ext-max-width ext-maxw-align-center" style="color:#363d44;line-height:1.3;max-width:1416px"><strong>Your website should beautifully represent your brand, help your dream clients find you, and give visitors a clear path to becoming customers. We're here to help.</strong></p></p>

<div class="wp-block-button dark mb-4 mt-4 hustle-cta-button is-content-justification-center text-center" form-id="2"><a form-id="2"class="wp-block-button__link no-border-radius">Sign Me Up!</a></div>
                    <div class="form-2 hustle-cta hustle-ui hustle-inline hustle-palette--gray_slate hustle_module_id_1 module_id_1 hustle-show" style="display: none" data-id="1" data-render-id="0" data-tracking="enabled" data-intro="no_animation" data-sub-type="shortcode" style="opacity: 1;">
                        <div class="hustle-inline-content">
                        <div class="hustle-optin hustle-optin--default">
                            <div class="hustle-success" data-close-delay="false" style="display: none;">
                                <span class="hustle-icon-check" aria-hidden="true"></span>
                                <div class="hustle-success-content"></div>
                            </div>
                            <div class="hustle-layout">
                                <div class="hustle-layout-body">
                                    <form class="hustle-layout-form" novalidate="novalidate">
                                    <div class="hustle-form hustle-form-inline">
                                        <div class="hustle-form-fields hustle-proximity-joined">
                                            <div class="hustle-field hustle-field-icon--static hustle-field-required opt-in-label"><label for="hustle-field-email-module-1" id="hustle-field-email-module-1-label" class="hustle-screen-reader">Enter your email...</label><input id="hustle-field-email-module-1" onfocus="hidePlaceholder(this)" type="email" class="hustle-input " name="email" value="" aria-labelledby="hustle-field-email-module-1-label" data-validate="1" data-required-error="Your email is required." data-validation-error="Please enter a valid email."><span class="hustle-input-label" aria-hidden="true"><span class="hustle-icon-email"></span><span>Enter your email...</span></span></div>
                                            <button class="hustle-button hustle-button-submit " type="submit" aria-live="polite" data-loading-text="Form is being submitted, please wait a bit."><span class="hustle-button-text">Keep Me Posted</span><span class="hustle-icon-loader hustle-loading-icon" aria-hidden="true"></span></button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="hustle_module_id" value="1"><input type="hidden" name="post_id" value="174"><input type="hidden" name="hustle_sub_type" value="shortcode">
                                    <div class="hustle-error-message" style="display: none;" data-default-error="Something went wrong, please try again."></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
<p class="has-white-color has-text-color"><p class="has-white-color has-text-color has-custom-lineheight ext-max-width ext-maxw-align-center" style="line-height:1.3;max-width:1416px">If you are a business owner and you want the ideal website platform that's easy to use, affordable, and can grow as your company grows, look no further. If you want to understand what to write on your website, how to entice your dream customers, and how to structure your web pages so you turn visitors into clients, we are here to help.</p></p>

<p class="has-white-color has-text-color"><p class="has-white-color has-text-color has-custom-lineheight ext-max-width ext-maxw-align-center" style="line-height:1.3;max-width:1416px">Join the waitlist and you'll be the first to know as soon as WebWiskee goes live.</p></p>

<div class="wp-block-button light my-5 text-center hustle-cta-button is-content-justification-center" form-id="3"><a form-id="3" class="wp-block-button__link no-border-radius">Take your Shot!</a></div>
                    <div class="form-3 hustle-cta hustle-ui hustle-inline hustle-palette--gray_slate hustle_module_id_1 module_id_1 hustle-show" style="display: none" data-id="1" data-render-id="0" data-tracking="enabled" data-intro="no_animation" data-sub-type="shortcode">
                        <div class="hustle-inline-content">
                        <div class="hustle-optin hustle-optin--default">
                            <div class="hustle-success" data-close-delay="false" style="display: none;">
                                <span class="hustle-icon-check" aria-hidden="true"></span>
                                <div class="hustle-success-content"></div>
                            </div>
                            <div class="hustle-layout">
                                <div class="hustle-layout-body">
                                    <form class="hustle-layout-form" novalidate="novalidate">
                                    <div class="hustle-form hustle-form-inline">
                                        <div class="hustle-form-fields hustle-proximity-joined">
                                            <div class="hustle-field hustle-field-icon--static hustle-field-required opt-in-label"><label for="hustle-field-email-module-1" id="hustle-field-email-module-1-label" class="hustle-screen-reader">Enter your email...</label><input id="hustle-field-email-module-1" onfocus="hidePlaceholder(this)" type="email" class="hustle-input " name="email" value="" aria-labelledby="hustle-field-email-module-1-label" data-validate="1" data-required-error="Your email is required." data-validation-error="Please enter a valid email."><span class="hustle-input-label" aria-hidden="true"><span class="hustle-icon-email"></span><span>Enter your email...</span></span></div>
                                            <button class="hustle-button hustle-button-submit " type="submit" aria-live="polite" data-loading-text="Form is being submitted, please wait a bit."><span class="hustle-button-text">Keep Me Posted</span><span class="hustle-icon-loader hustle-loading-icon" aria-hidden="true"></span></button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="hustle_module_id" value="1"><input type="hidden" name="post_id" value="174"><input type="hidden" name="hustle_sub_type" value="shortcode">
                                    <div class="hustle-error-message" style="display: none;" data-default-error="Something went wrong, please try again."></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
<?php get_footer() ?>
