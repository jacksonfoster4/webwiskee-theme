/*
 * Template Name: About Page
 */

<!-- hero -->

<!-- about section with watermark -->
<div class="container-fluid about-intro">
    <div class="p-5" style="position: relative">
        <img src="/wp-content/uploads/2021/04/Rings-Watermark-White-20.png" class="about-watermark">
        <div class="row justify-content-md-center">
            <div class="col-lg-8 col-md-10">
                <h1 class="text-center text-white">
                    Most DIY website services make big promises but leave you with an unexpectedly high bar tab.
                </h1>
                <p>
                    The team behind WebWiskee worked in the digital marketing space for years, and we saw first hand all kinds of horror stories that our clients had endured. It. Hurt. Our. Hearts.
                </p>
                <ul class="m-0 mt-4 p-0">
                    <li>DIY websites that promised to be cheap to make and easy to use, but turned out to be neither.</li>
                    <li>Website developers who knew nothing about design, ADA compliance, or how Google works.</li>
                    <li>Money wasted on overly expensive sites that didn't target their ideal customer.</li>
                    <li>Websites held hostage by the developer, who would take weeks or months to respond to simple update requests.</li>
                    <li>Years wasted not progressing in their business because of their bad online presence.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<h2>WebWiskee was created to champion small businesses and help them leave their mark.</h2>
<p>We believe you shouldn't need to spend a fortune to create a memorable site. Our platform was designed to help you start your business on the right foot, with a website that you can actually build yourself, and with access to ongoing support to scale as you grow.</p>


<!-- two column, cover image-->
<div class="container-fluid p-0 two-column-cover-image">
    <div class="row w-100 m-0">
        <div class="col-md-6 image-col" style="background-image: url('/wp-content/uploads/2020/11/the-quick-guide-to-nofollow-links-twitter.png');"></div>
        <div class="col-md-6 content-col text-center">
            <span class="mb-3"><h2>Leading With<br>Integrity.</h2></span>
            <span><h2 class="m-0">Guiding you to<br>Growth.</h2></span>
        </div>
    </div>
</div>

<h2>Here's what we believe</h2>
<style>
  .watermarked-columns .col-lg-3 {
  z-index: -1;
  color: white;
  background-color: #344860;
}
.watermarked-columns .col-lg-3 {
  position: relative
}
.watermarked-columns img {
  position: absolute;
  z-index: -2;
  bottom: -40%;
  height: 100%;
}

.watermarked-columns .center-watermark {
  width: 100%
}

.watermarked-columns h1 {
  color: white;
  font-size: min(3.3vw, 2rem) !important;
}
.watermarked-columns p {
  color: white;
  font-size: 1.3rem;
}
@media only screen and (max-width: 1400px){
  .watermarked-columns img {
    display: none
  }
  .watermarked-columns h1 {
    font-size: max(3.3vw, 2rem) !important;
  }
  .watermarked-columns p {
    line-height: 1.3
  }
}
</style>
<div class="container watermarked-columns mt-5">
    <div class="row justify-content-center" style="overflow: hidden; position: relative">
        <div class="col-lg-3 col-md-8 px-0 py-3 mx-2 my-3">
            <img style=" right:0;" src="/wp-content/uploads/2021/04/Rings_White_Left_20.png">
            <div class="watermark-overlay"></div>
            <div class="p-4">
                <h1 class="text-center">Integrity</h1>
                <p>
                    We're the team you can trust behind the counter to do the right thing even when no one's looking. We appreciate that you're looking to use to be your guide, and we take that seriously.
                </p>
            </div>
        </div>
        <div class="col-lg-3 col-md-8 px-0 py-3 mx-2 my-3">
            <img style="left:0;" class="center-watermark" src="/wp-content/uploads/2021/04/Rings_White_Center_20.png">
            <div class="watermark-overlay"></div>
            <div class="p-4">
                <h1 class="text-center">Adaptability</h1>
                <p>
                    You website needs to be uniquely you. We let you mix and match from a wide range of features so you're never stuck choosing from packages that limit your possibilities. As technologies evolve, so will we, so your website won't be stuck with outdated tech.
                </p>
            </div>
        </div>
        <div class="col-lg-3 col-md-8 px-0 py-3 mx-2 my-3">
            <img style="left:0;" src="/wp-content/uploads/2021/04/Rings_White_Right_20.png">
            <div class="watermark-overlay"></div>
            <div class="p-4">
                <h1 class="text-center">Growth</h1>
                <p>
                    We were built on the belief that your business and website should get better with age. WebWiskee makes that easier than ever. Finally.
                </p>
            </div>
        </div>
    </div>
</div>
