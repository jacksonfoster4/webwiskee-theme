<?php
/**
* Template Name: Pricing Table
*/
get_header();
?>
<div class="pricing-template">
<!-- COVER HERO -->
<!-- hero with watermark -->
<div class="jumbotron-fluid pricing-hero" style="background-image: url('/wp-content/uploads/2021/05/Simpson_Portraits_WEB_SFS_3389-3.jpg')">
    <div class="container">
        <div class="row justify-content-md-start">
            <div class="jumbotron-col col-md-5 col-sm-12">
                <div class="hero-content float-start text-start">
                    <h1>Pricing</h1>
                    <p>Our pricing tiers are based on your storage and user needs... everything else can be added as you need it or want it.</p> 
                </div>
            </div>
        </div>
    </div>
    <img class="watermark" src="/wp-content/uploads/2021/04/Rings-Watermark-White-20.png">
    <div class="overlay"></div>
</div>
<!-- END COVER HERO -->

<!-- content section -->

<div class ="container beta-testing">
    <div class="row">
        <div class="col-12 py-5">
            <h2>WebWiskee is still in closed Beta testing, but we're opening to the public soon! </h2>
            <p>We get lots of questions about our launch timeline (soon!) and our pricing (super affordable!).</p>
<p>Some features are still in development, but our goal is to provide you with super transparent pricing. Hidden charges that sneak up on you are no fun, so we're doing our darndeset to make sure that what you see is what you get... and that you know exactly what you're paying for.</p>
        </div>
    </div>
</div>

<!-- end content section -->

<!-- pricing section -->
<div class="container-fluid px-0 pricing-table-container py-5 position-relative">
    <div class="row position-relative">
        <div class="col-12">
            <?php echo do_shortcode('[wu_pricing_table]'); ?>
        </div>
    </div>
    <div id="blue-background"></div>
</div>
<!-- end pricing section -->


<!-- features coming soon -->
<div class="container-fluid px-0 coming-soon py-5">
    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <h2>FEATURES COMING SOON:</h2>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-2 col-sm-6 col-10">
                        <img class="feature-image" src="/wp-content/uploads/2021/05/elearning.png" /> 
                        <p> eCourses & eLearning</p>
                    </div>
                    <div class="col-md-2 col-sm-6 col-10">
                        <img class="feature-image" src="/wp-content/uploads/2021/05/id.png" /> 
                        <p>Membership sites</p>
                    </div>
                    <div class="col-md-2 col-sm-6 col-10">
                        <img class="feature-image" src="/wp-content/uploads/2021/05/money.png" /> 
                        <p>Subscription & recurring payments</p>
                    </div>
                    <div class="col-md-2 col-sm-6 col-10">
                        <img class="feature-image" src="/wp-content/uploads/2021/05/dollars.png" /> 
                        <p>Gift Card sales</p>
                    </div>
                    <div class="col-md-2 col-sm-6 col-10">
                        <img class="feature-image" src="/wp-content/uploads/2021/05/s.png" /> 
                        <p>Point of sale</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end features coming soon -->

<!-- availabe payment processors -->
<div class="container py-5 payment-processors">
    <div class="row justify-content-center">
        <div class="col-md-10 col-xs-12 text-center">
            <h2>AVAILABLE PAYMENT PROCESSORS</h2>
            <div class="row text-center justify-content-center">
                <div class="col-md-4 col-xs-12">
                    <div class="px-2 py-2">
                        <img src="/wp-content/uploads/2021/05/stripe.png" />
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="px-4 py-2">
                        <img src="/wp-content/uploads/2021/05/paypal.png" />
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="px-2 py-2">
                        <img class="square-logo" src="/wp-content/uploads/2021/05/square.png" />
                    </div>
                </div>
                <p class="mt-3">Payment processing fees will apply and are determined by the individual processor</p>
            </div>
        </div>
    </div>
</div>
<!-- end availabe payment processors -->


<!-- faqs --> 
<?php
$faqs = [
   
    "What is WebWiskee?" => 

    "WebWiskee is a way to create your own website using WordPress. Our do-it-yourself website builder provides you with the simplest way to build a WordPress website. We provide you with access to templates and tools so you can create a website or create a blog with no web development experience needed.",

    "What does a WebWiskee plan include?" => 

    "All plans include access to our website templates, hosting, maintenance and security monitoring. The different tiers are primarily based on how much storage and how many user roles you need for your site. Additional features and tools you may want can be added on as they become available.  Mix your own drink, so to speak!",

    "How much will it cost to build my WebWiskee site?" => 

    "It depends on what you need. You’ll start by selecting a base plan that’s in line with your website needs. And unlike most website builders that make you buy features in bulk (many of which you’ll never end up using!), we let you choose from a wide range of features to add as you need them. Our platform makes it easier than ever for you to easily scale your website as your business grows and evolves.",

    "How do I know how much storage I need?" => 

    "An average web page is about 3MB each. Our Starter Plan is great for small to mid-sized businesses who don’t need eCommerce and who don’t have an image heavy website.  Our Pro Plan is perfect for larger sites with lots of images or downloadable files. Our Top Shelf Plan is for large, image heavy sites and lots of large downloadable content.",

    "Does WebWiskee have a free trial?" =>

    "Yes! WebWiskee has a two week free trial period.  Take your shot and check out the builder and the features and make sure you like it. If you decide it’s not for you, just cancel your account within that two week period to avoid the first month’s charge.",

    "When will more features be added?" => 

    "WebWiskee is still a work in progress, and we’ll be rolling out new features as they are tested and deemed ready. We plan to continually add new features and improvements as we go.",

    "Why do you only have basic eCommerce available?" => 

    "WebWiskee is still in Beta, so we want to make sure the basics are working perfectly before adding more complex eCommerce features.", 

    "Can I embed videos, calendars or other 3rd party tools?" => 

    "Of course! If you’ve got embed code, you can use it on your WebWiskee site. YouTube or Vimeo videos, Calendly, Acuity, whatever you need, just grab that embed code."
]; ?>

<div class="container-fluid px-0 py-5 faqs-container">
    <div class="row justify-content-center">
        <div class="col-md-10 col-xs-12">
            <h2 class="text-center">FAQ</h2>
            <div class="p-2 accordion" id="accordionExample"> <?php
                $index = 0;
                foreach($faqs as $question => $answer): ?>
                    <div class="accordion-item">
                        <h2 class="p-0 accordion-header" id="heading<?php echo $index ?>">
                          <button class="accordion-button <?php echo $index != 0 ? 'collapsed' : '' ?>" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $index ?>" aria-expanded="true" aria-controls="collapse<?php echo $index ?>">
                    <?php echo $question; ?>
                          </button>
                        </h2>
                        <div id="collapse<?php echo $index ?>" class="accordion-collapse collapse <?php echo $index == 0 ? 'show': ''?>" aria-labelledby="heading<?php echo $index ?>" data-bs-parent="#accordionExample">
                          <div class="accordion-body">
                                <?php echo $answer ?>
                          </div>
                        </div>
                      </div>
                <?php $index += 1 ?>
                <?php endforeach; ?>
            </div>
        </div>
     </div>
</div>
<!-- end faqs -->

<script>
        jQuery(document).ready(reposition);
        window.addEventListener('resize', reposition);

        function countLines(el) {
            let lineHeight = window.getComputedStyle(el).getPropertyValue('line-height')
            let divHeight = el.offsetHeight
            lineHeight = parseInt(lineHeight);
            let lines = divHeight / lineHeight;
            return lines
        }
        function reposition(){
            // repositions blue background behind pricing table to line up with top of features table
            let background = document.getElementById("blue-background");
            let specs = document.getElementsByClassName('specs')[0].getBoundingClientRect();
            let container = document.getElementsByClassName("pricing-table-container")[0];
            let containerPadding = window.getComputedStyle(container, null).getPropertyValue("padding-top")
            container = container.getBoundingClientRect();
            let height = (specs.top - container.top)
            background.style.height = height+'px'
            
            // this makes sure all descriptions have the same number of lines so
            // layout isnt messed up
            let descriptions = document.querySelectorAll(".plan-content p")
            let mostLines = 0;

            // find out the most number of lines present
            Array.from(descriptions).map(desc => {
                desc.innerHTML = desc.innerHTML.replaceAll("<br>", "")
                if(countLines(desc) > mostLines){
                    mostLines = countLines(desc)
                }
            })

            // if the element text has less lines than
            // the most, add <br> until they are equal
            Array.from(descriptions).map(desc => {
                if(countLines(desc) < mostLines){
                    while(countLines(desc) < mostLines){
                       desc.innerHTML += '<br>' 
                    }
                } 
            })
        }
</script>
</div>
<?php get_footer();
?>



