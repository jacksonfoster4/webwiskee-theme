<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		
		<?php
		if ( has_post_thumbnail() ) :
			echo '<div class="post-thumbnail">' . get_the_post_thumbnail() .'</div>';
		endif;
		if ( is_singular() ) :
			the_title( '<div class="entry-title text-center">', '</div>' );
		else :
			the_title( '<div class="entry-title text-center"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></div>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta text-center pt-3">
			<?php //gutenberg_starter_theme_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	<?php
	
	?>
	<div class="entry-content">
		<?php
		$read_more = '<a class="read-more" href="' . get_the_permalink() . '"> Read More...</a>';
		echo wp_trim_words(
			get_the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gutenberg-starter-theme' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) ), 105, $read_more);

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gutenberg-starter-theme' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php // gutenberg_starter_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
