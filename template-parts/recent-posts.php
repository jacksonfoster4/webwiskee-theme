<?php
        // pick 3 random posts out of lastest 10
        add_filter( 'the_posts', function( $posts, \WP_Query $query )
        {
            if( $pick = $query->get( '_shuffle_and_pick' ) )
            {
                shuffle( $posts );
                $posts = array_slice( $posts, 0, (int) $pick );
            }
            return $posts;
        }, 10, 2 );
    
        $args = [
            'post_type'             => 'post',
            'posts_per_page'        => 10,
            'orderby'               => 'date',
            'order'                 => 'DESC',
            'no_found_rows'         => 'true',
            '_shuffle_and_pick'     => 3 // <-- our custom argument
        ];
        
        $recent_posts = new \WP_Query( $args );
        
        if($recent_posts->have_posts()): ?>
            <h5 class="mt-5">Recent Posts </h5> <?php
            while($recent_posts->have_posts()):
                $recent_posts->the_post();

                if(has_post_thumbnail()):
                    the_post_thumbnail();
                endif;

                echo "<a class='recent-post-title' href='". get_the_permalink(). "'>". get_the_title() . "</a>";
                
            endwhile;
        endif;
?>