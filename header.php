<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenberg-starter-theme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

	<?php wp_head();
	$post_id = get_queried_object_id(); 
	?>
	<style>
		<?php
		if(get_post_meta($post_id, 'hide_header')){
			echo "#ww-header {display: none !important}";
		}
		if(get_post_meta($post_id, 'hide_footer')){
			echo "#ww-footer {display: none !important}";
		}?>
	</style>
</head>

<body <?php body_class(); ?>>
<?php 
	function add_additional_class_on_li($classes, $item, $args) {
		if(isset($args->li_class)) {
			$classes[] = $args->li_class;
		}
		return $classes;
	}
	
	function add_menu_link_class( $atts, $item, $args ) {
		if (property_exists($args, 'a_class')) {
		  $atts['class'] = $args->a_class;
		}
		return $atts;
	  }

	add_filter('nav_menu_css_class', 'add_additional_class_on_li', 10, 3);
	add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 10, 3);
?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'gutenberg-starter-theme' ); ?></a>
	<nav class="navbar navbar-expand-lg ww-header <?php echo $args['remove_transparency'] || get_post_meta($post_id, 'remove_transparency') ? 'ww-no-transparency': '' ?> <?php echo get_post_meta($post_id, 'remove_transparency') || $args['remove_transparency'] ? 'sticky' : '' ?> " id="ww-header">
		<div class="container-fluid">
	  		<div class="row w-100 m-0">
               <div class="col-4 <?php echo get_post_meta($post_id, 'full_color_logo') ? 'full-color-logo' : '' ?>">
				  <?php the_custom_logo(); ?>
				</div>
				<div class="col-8 text-end" style="display: flex;justify-content: center;align-items: end; flex-direction: column">
	  				<div class="w-100">
					  <button class="navbar-toggler collapsed" id='toggler' type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					</div>
					<div class="w-100">
						<div class="navbar-nav-container">
							<div class="collapse navbar-collapse" id="navbarNav">
								<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
											'container_class'=> 'ms-auto',
											'menu_class'     => 'navbar-nav',
											'li_class'       => 'nav-item mx-4'	,
											'a_class'        => 'nav-link'				
										) );
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</nav>
	<script>
		document.getElementById('toggler').addEventListener('click', (e) => {
			let header = document.getElementById("ww-header")
			if(Array.from(header.classList).includes('gradient-background')){
				header.classList.remove('gradient-background')
				if(window.scrollY <= 5){
					header.classList.remove('sticky')
				}

			}
			else {
				header.classList.add('gradient-background')
				header.classList.add('sticky')
			}
		})
		
		document.addEventListener('scroll', toggleSticky)

		function toggleSticky(){
			let header = document.getElementById("ww-header")
			if(document.querySelector('.ww-no-transparency')){
				return
			}
			if(window.scrollY > 5){
				header.classList.add('sticky')
			}
			else {
				header.classList.remove('sticky')
			}
		}
		function addPaddingToContainer(e){
			let header = document.getElementById('ww-header')
			let height = header.getBoundingClientRect().height
			let container = document.getElementById('primary')
			if(Array.from(header.classList).includes('ww-no-transparency')){
				container.style = `padding-top: ${height}px`
			}
		}
        document.addEventListener('DOMContentLoaded', addPaddingToContainer)
		document.addEventListener('resize', addPaddingToContainer)
        document.addEventListener('DOMContentLoaded', addCTAListener)

		function addCTAListener(){
			let cta = document.getElementsByClassName('hustle-cta-button')
			Array.from(cta).map(el => {
				el.addEventListener('click', showEmailForm)
			})
		}
		let elWithForm = []
		function showEmailForm(e){
			console.log('test')
			let formId=e.target.getAttribute('form-id')
			document.querySelector(`.form-${formId}`).style.display = 'block'
			e.stopPropagation()
		}

		function hidePlaceholder(e){
			let parent = e.closest('.hustle-form-fields')
			parent.querySelector('.hustle-icon-email').style.display = 'none'
			Array.from(parent.querySelectorAll('.hustle-input-label span')).map(el => el.style.display = 'none')
		}
		
    </script>
