<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gutenberg-starter-theme
 */

get_header(null, ['remove_transparency' => true]); ?>

	<main id="primary" class="site-main">

	<?php
	while ( have_posts() ) : the_post(); ?>
		<div class="container" id="ww-single-post-container">
			<div class="row justify-content-center py-5">
				<div class="col-md-8 col-sm-10 py-4"><?php
					the_title( '<div class="entry-title text-center">', '</div>' );?>
					<?php
					the_content(); 
					// If comments are open or we have at least one comment, load up the comment template.
					// setting wasn't working for some reason.
					//if ( comments_open() || get_comments_number() ) :
					//	comments_template();
					//endif; ?>
				</div>
				<div class="col-md-2 col-sm-10 py-4 ps-3 text-center">
                    <div class="blog-social-icons mb-4">
                        <div class="dashicons-wrapper-dark">
                            <a href="<?php echo get_option('ww_facebook_link'); ?>" target="_blank" class="dashicons dashicons-facebook-alt"></a>
                        </div>
                        <div class="dashicons-wrapper-dark">
                            <a href="<?php echo get_option('ww_instagram_link'); ?>" target="_blank" class="dashicons dashicons-instagram"></a>
                        </div>
                        <div class="dashicons-wrapper-dark">
                            <a href="<?php echo get_option('ww_twitter_link'); ?>" target="_blank" class="dashicons dashicons-twitter"></a>
                        </div>
                    </div>

                    <?php 
                    get_search_form();
                    get_template_part( 'template-parts/recent-posts'); ?>

				</div>
			</div>
		</div><?php

		the_post_navigation( array(
			'prev_text' => '&larr; %title',
			'next_text' => '%title &rarr;',
		) );

		

	endwhile; // End of the loop.
	?>

	</main><!-- #primary -->

<?php
get_footer();
