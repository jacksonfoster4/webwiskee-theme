<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenberg-starter-theme
 */

?>
<footer class="ww-footer py-4" id="ww-footer">
	<div class="container text-center pt-4">
		<div class="row justify-content-center">
			<div class="col-lg-4 col-md-10 col-sm-12 d-flex order-xl-1 order-md-2" style="justify-content: center;align-items: center;">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer-menu',
							'menu_id'        => 'footer-menu',
							'container_class'=> 'mt-4',
							'menu_class'     => 'navbar-nav footer-links',
							'li_class'       => 'nav-item mx-4'	,
							'a_class'        => 'nav-link'				
						) );
					?>
			</div>
			<div class="col-lg-4 col-md-10 col-sm-12 order-xl-2 order-lg-2">
				<a href="/">
					<!-- add to theme options backend -->
					<img class="footer-image" src="<?php echo get_option("ww_footer_logo"); ?>" />
				</a>
				<!-- add to theme options backend -->
				<div class="pt-4 pb-3"><?php echo get_option("ww_footer_content"); ?></div>
				<!-- add to theme options backend -->
				<div class="footer-social-icons">
					<div class="dashicons-wrapper">
						<a href="<?php echo get_option('ww_facebook_link'); ?>" target="_blank" class="dashicons dashicons-facebook-alt"></a>
					</div>
					<div class="dashicons-wrapper">
						<a href="<?php echo get_option('ww_instagram_link'); ?>" target="_blank" class="dashicons dashicons-instagram"></a>
					</div>
					<div class="dashicons-wrapper">
						<a href="<?php echo get_option('ww_twitter_link'); ?>" target="_blank" class="dashicons dashicons-twitter"></a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-10 col-sm-12 d-flex email-col order-last"style="justify-content: center;align-items: center;">
				<a href="https://webwiskee.activehosted.com/f/1" target="_blank" class="wp-block-button__link no-border-radius">Apply to be a Beta Tester</a>
			</div>
		</div>
		<p class="footer-copyright">Copyright 2021 WebWiskee, LLC <span class="mx-2">|</span> <a href="mailto:hello@webwiskee.com">hello@webwiskee.com</a></p>
	</div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>
