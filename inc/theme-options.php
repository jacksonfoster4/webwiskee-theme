<?php 

/**
 * Set up a WP-Admin page for managing turning on and off theme features.
 */
function gutenberg_starter_theme_add_options_page() {
	add_theme_page(
		'Theme Options',
		'Theme Options',
		'manage_options',
		'gutenberg-starter-theme-options',
		'gutenberg_starter_theme_options_page'
	);

	// Call register settings function
	add_action( 'admin_init', 'gutenberg_starter_theme_options' );
	add_action( 'load-post.php', 'custom_meta_box_setup' );
	add_action( 'load-post-new.php', 'custom_meta_box_setup' );
}

add_action( 'admin_menu', 'gutenberg_starter_theme_add_options_page' );

function custom_meta_box_setup(){
	add_action("add_meta_boxes", "add_custom_meta_box");
	add_action( 'save_post', 'save_custom_meta', 10, 2 );
}

function save_custom_meta($post_id, $post){
	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
	return $post_id;

	$hide_header = ( isset( $_POST['hide-header'] ) ? $_POST['hide-header'] : null );
	$hide_footer = ( isset( $_POST['hide-footer'] ) ? $_POST['hide-footer'] : null );
	$remove_transparency = ( isset( $_POST['remove-transparency'] ) ? $_POST['remove-transparency'] : null );
	$full_color_logo = ( isset( $_POST['full-color-logo'] ) ? $_POST['full-color-logo'] : null );

	if($hide_header){
		update_post_meta($post_id, 'hide_header', true);
	}
	else {
		delete_post_meta( $post_id, 'hide_header');	
	}

	if($hide_footer){
		update_post_meta($post_id, 'hide_footer', true);
	}
	else {
		delete_post_meta( $post_id, 'hide_footer');	
	}

	if($remove_transparency){
		update_post_meta($post_id, 'remove_transparency', true);
	}
	else {
		delete_post_meta( $post_id, 'remove_transparency');	
	}
	if($full_color_logo){
		update_post_meta($post_id, 'full_color_logo', true);
	}
	else {
		delete_post_meta( $post_id, 'full_color_logo');	
	}
}

function add_custom_meta_box(){
	add_meta_box('ww_header_footer_display', 'Header & Footer Display Options', 'header_footer_display_options');
}

function header_footer_display_options(){ 
	global $post;
	?>
	<form method="post">
		<div>
			<p>Hide Header?</p>
			<input type="checkbox" name="hide-header" value="true" <?php echo get_post_meta($post->ID, 'hide_header') ? 'checked' : ''; ?> /><br>
			<p>Hide Footer?</p>
			<input type="checkbox" name="hide-footer" value="true" <?php echo get_post_meta($post->ID, 'hide_footer') ? 'checked' : ''; ?> />
			<p>Remove Transparency</p>
			<input type="checkbox" name="remove-transparency" value="true" <?php echo get_post_meta($post->ID, 'remove_transparency') ? 'checked' : ''; ?> />
			<p>Use Full Color Logo in Header</p>
			<input type="checkbox" name="full-color-logo" value="true" <?php echo get_post_meta($post->ID, 'full_color_logo') ? 'checked' : ''; ?> />
		</div>
	</form>

<?php }
/**
 * Build the WP-Admin settings page.
 */
function gutenberg_starter_theme_options_page() { ?>
	<div class="wrap">
		<form enctype="multipart/form-data" method="post">
			<textarea name="footer-content"><?php echo get_option('ww_footer_content'); ?></textarea>
			<input type="text" placeholder="Footer Logo" name="footer-logo" value="<?php echo get_option('ww_footer_logo'); ?>" />
			<input type="text" placeholder="Facebook Link" name="facebook-link"value="<?php echo get_option('ww_facebook_link'); ?>" />
			<input type="text" placeholder="Twitter Link" name="twitter-link"value="<?php echo get_option('ww_twitter_link'); ?>" />
			<input type="text" placeholder="Instagram Link" name="instagram-link"value="<?php echo get_option('ww_instagram_link'); ?>" />
			<button type="submit">Submit</button>
		</form>
	</div>
<?php 

if(!empty($_POST)){
	$footer_content = $_POST['footer-content'];
	$footer_logo = $_POST['footer-logo'];
	$facebook = $_POST['facebook-link'];
	$twitter = $_POST['twitter-link'];
	$instagram = $_POST['instagram-link'];

	if(!empty($_FILES)){
		wp_handle_upload($_FILES);
	}
	if($footer_content){
		update_option('ww_footer_content', $footer_content);
	}
	if($footer_logo){
		update_option('ww_footer_logo', $footer_logo);
	}
	if($facebook){
		update_option('ww_facebook_link', $facebook);
	}
	if($twitter){
		update_option('ww_twitter_link', $twitter);
	}
	if($instagram){
		update_option('ww_instagram_link', $instagram);
	}
}

}


